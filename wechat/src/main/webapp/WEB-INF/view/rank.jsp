<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- xiezc 亲手所写的样式  -->
<link rel="stylesheet"	href="/css/stylesheets/style.css">
<script src="http://apps.bdimg.com/libs/jquery/1.10.2/jquery.min.js"></script>
<script	src="http://apps.bdimg.com/libs/jquerymobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
<!-- xiezc 亲手所写的js -->
<script src="/js/script-1-5.js"></script>	
<title>简单就好</title>
</head>
<body>
	<div data-role="page" id="page1">
		<div  class="header">
			<h1>我的地盘儿，我的歌
			</h1>
		</div>
		<div  class="ui-content">
			<ul id="uldata" >
				
			</ul>
		</div>
		<div class="player" >
			<!-- 左 -->
			<div class="ldiv"><div class="img"></div></div>
			<!-- 中-->
			<div class="cdiv">
				<p class="sname" ></p>
				<p class="singer" ></p>
			</div>
			<!-- 右 -->
			<div class="rdiv">
			 	<img class="icon1" alt="上一首" src="/css/images/rewind.png"/>
			 	<img class="icon2" alt="播放" src="/css/images/play.png" />
			 	<img class="icon3" alt="下一首" src="/css/images/fastforward.png"/>
			 	<br>
			  	<span class="currtTime">0:00</span>
			</div>
		</div>
	</div>
</body>
</html>