(function ($) {
    //适用于只是输入费用归属公司的方法
    $.fn.extend({
        selectInput: function () {
            var url = "/js/a.json";
            var input = $(this);
            input.attr("autocomplete", "off");//关闭输入框自带的历史数据补全功能
            var oldValue = "";

            var width = $(this).width() + "px";
            var top = $(this).position().top + 30 + "px";
            var left = $(this).position().left + "px";
            console.log(width+"    "+top+"     "+left);
            var style = '.select4_box{border: 1px solid #5897fb;position: absolute;width:250px;background: #fff;'
                + 'border-radius: 4px;-webkit-box-shadow: 0 4px 5px rgba(0, 0, 0, .15);'
                + 'box-shadow: 0 4px 5px rgba(0, 0, 0, .15);z-index: 9999;top:' + top + ';left:' + left + ';width:' + width + ';}'
                + '.select4_box ul{padding: 0px;margin: 5px;}'
                + '.select4_box ul li{list-style: none;padding: 3px 7px 4px; cursor: pointer;}'
                + '.select4_box ul li:hover{background: #51A9A9;color: #fff;}'
                + '.select4_box ul li.active{background: #3875d7;'
                + 'color: #fff;}'
            $(this).before('<style>' + style + '</style>');

            $(this).bind("click", function () {
                var val = $(this).val();
                console.log(val);

                if (oldValue == val) {
                    return;
                }
                $.get(url, { "supplierName": val }, function (data) {
                    if (data.length > 0) {
                        var insertHtml = '<div class="select4_box"><ul>';
                        $.each(data, function (i, v) {
                            insertHtml += '<li data="v.code">' + v.name + '</li>';
                        });
                        insertHtml += "</ul></div>";
                        input.after(insertHtml);//
                    } else {
                        alert("未找到你输入的费用归属公司,请检查输入是否正确.");
                    }

                }, "json");

            });

            $(this).keyup(function (event) {
                if (event.keyCode == 40) {
                    var index = $(".select4_box li.active").index() + 1;
                    $(".select4_box li").eq(index).addClass('active').siblings().removeClass('active');
                    input.val($(".select4_box li.active").text());
                    oldValue = $(".select4_box li.active").text();
                } else if (event.keyCode == 38) {
                    var index = $(".select4_box li.active").index() - 1;
                    if (index < 0) {
                        index = $(".select4_box li").length - 1;
                    }
                    $(".select4_box li").eq(index).addClass('active').siblings().removeClass('active');
                    input.val($(".select4_box li.active").text());
                    oldValue = $(".select4_box li.active").text();
                } else if (event.keyCode == 13) {
                    event.stopPropagation();
                    //alert($(".select4_box li.active").text());
                    input.val($(".select4_box li.active").text());
                    oldValue = $(".select4_box li.active").text();
                    $(".select4_box").remove();
                    return false;
                } else {
                    input.trigger("click");
                }
            });

            $(document).bind("click", ".select4_box li", function () {
                input.val($(this).text());
                oldValue = $(".select4_box li.active").text();
                $(".select4_box").remove();
            });

            $(document).click(function (event) {
                $(".select4_box").remove();
            });

            $(".select4_box").click(function (event) {
                event.stopPropagation();
            });
        }
    });
})(jQuery)