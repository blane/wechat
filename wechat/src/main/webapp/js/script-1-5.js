﻿$(document).ready(function(){
		var pageNo=1;
		var interval;//定时任务
		load();//加载首页的歌曲
		var audion = document.createElement("AUDIO");
		
		/*
		*点击播放按钮播放音乐
		*/
		$("#uldata").on("tap","li .play2",function(){
			if($(this).hasClass("play2click")){
					pause(this);//暂停
					//clearPauseButton(this);
			}else{
				play(this);//开始播放
				//nextPlay();
				clearPlayButton(this);
			}
		});
		
		/*
		* 点击底部控制区的播放图标
		*/
		$(".player").on("click","img.icon2",function(){
			pause(this);
		});
		
		/*
		* 点击底部的控制区的下一曲图标
		*/
		$(".player").on("click","img.icon3",function(){
			nextPlay();
		});
		
		/*
		* 点击底部的控制区的上一曲图标
		*/
		$(".player").on("click","img.icon1",function(){
			beferPlay();
		});
		
		/*
		* 播放上一首歌
		*/
		function beferPlay(){
			//获得当前播放的歌曲的li
			var li= $(".play2click").parent().parent();
			//获得上一首歌的li
			var prevli=li.prev();
			//获得上一首歌的src
			var playbu= prevli.find("span.play2");
			
			if(typeof(playbu.html())=='undefined'){//到顶了
				playbu=$("#uldata").find("li:first").find("span.play2");//到顶了，则播放第一首歌
			}
			//前面找到上一首后，播放
			play(playbu);
			clearPlayButton(playbu);
		}
		
		
		/*
		* 播放下一首歌曲
		*/
		function nextPlay(){
			//获得当前播放的歌曲的li
			var li= $(".play2click").parent().parent();
			//获得下一首歌的li
			var nextli=li.next();
			//获得下一首歌的src
			var playbu= nextli.find("span.play2");
			if(typeof(playbu.html())=='undefined'){//到底了
				playbu=$("#uldata").find("li:first").find("span.play2");//到底了，则播放第一首歌
			}
			//前面找到下一首后，播放
			play(playbu);
			clearPlayButton(playbu);
		}
		/*
		 * 暂停播放
		*/
		function pause(e){
			 var playerBottom=$(".player");//底部播放盒区域
			 var playImg= playerBottom.find("img.icon2");//获得播放的状态图片
			// var audion = $(audioDom); 
			 if(audion.src==''){
				 var firstPlay = $("#uldata").find("li:first").find("span.play2");
				 firstPlay.addClass("play2click");
				 firstPlay.text("暂停");
				 audion.src="/music?songName="+firstPlay.attr("data");//到底了，则播放第一首歌
			 }
			 var pauseF= audion.paused;//获得歌曲的播放与暂停状态
			 if(pauseF){
				//暂停状态
				$(e).text("暂停");
				 playImg.attr("src","/css/images/pause.png");
				 audion.play();
				 interval= setInterval(currentPlayTime, 1000); //重新计时 
			 }else{
				//播放状态
				 audion.pause();
				 clearInterval(interval);//清除计时任务
				 $(e).text("播放");
				 playImg.attr("src","/css/images/play.png");
			 }
		}
		
		/*
		* 播放歌曲，参数就是要播放歌曲的播放按钮
		*/
		function play(e){
			var src='/music?songName='+ $(e).attr("data");
			// var audion = $(audioDom);  
			 audion.src=src;//重置src
			 audion.load();//重新加载
			 audion.play();//开始播放
			 clearInterval(interval);//清除之前的计时任务
			 //更新播放的时间
			 interval= setInterval(currentPlayTime, 1000);  
		}
		
		/*
		* 此方法时间播放按钮的文字有播放改为暂停的方法
		* 清除上一个播放按钮的样式，改变现在的播放按钮的样式
		* 传入的参数是当前的按钮
		*/
		function clearPlayButton(e){
			 //更新播放按钮的样式和文字
			 var clickP= $(".play2click");
			 clickP.each(function(){
				 $(this).text("播放");
				 $(this).removeClass("play2click");
			 });
			 $(e).addClass("play2click");
			 $(e).text("暂停")
			 
			 var playerBottom=$(".player");//底部播放盒区域
			 var  singerspan=playerBottom.find(".singer");//获得歌手展示区
			 singerspan.html( $(e).attr("singer"));//更新歌手
			 var snameSpan=playerBottom.find(".sname");//获得歌手展示区
			 snameSpan.html($(e).attr("sname"));//更新歌名
			 var playImg= playerBottom.find("img.icon2");//获得播放的状态图片
			 playImg.attr("src","/css/images/pause.png");
		}
		
		/*
		* 定时任务，用于动态显示歌曲的播放时间
		*/
		function currentPlayTime(){
			var seeTime="0:00";
			//var audion=	$(audioDom);
			if(audion.autoplay){//是否就绪
				$(".currtTime").html("加载中.");
				return;
			}
			var time= audion.currentTime;//当前的播放时间
			var end= audion.ended;//判断是否结束
			if(end){//判断是否结束
				clearInterval(interval);//清除计时任务
				nextPlay();//播放下一首
			}
			var minute= parseInt(time/60);
			var secc=parseInt(time-minute*60);
			if(secc<10){
				secc="0"+secc;
			}
			seeTime=minute+":"+secc;
			$(".currtTime").html(seeTime);
		}
		
		/*
		*滚动到顶部刷新页面，滚动到底部加载一页的方法
		*/
		$("#uldata").on("scrollstop",function(e){
			var scrollTop= $(document).scrollTop(); //获取滚动条到顶部的垂直高度
			var bheight= $(document.body).height();//网页的高度
			var wheight= $(window).height();//可视窗口的高度
			if(scrollTop==0){
				 location.reload();
			}
			if((scrollTop+wheight)==bheight){
				load();
			}
		})
		
		/*
		*加载一页的方法
		*/
		function load(){
			var	pageSize=10;
			$.ajax({
				url:"/music?s="+Math.random(),
				type: "POST",
				dataType:"json",
				data:{"pageNo":pageNo,"pageSize":pageSize},
				success:function(result){
					var ul= $("#uldata");
				   	if(result.length==0){
				   		return;
				   	}
				   	pageNo++; 
					for(var index in result){
						song=result[index];	
						if(pageNo==2 && index==0 ){
							 var playerBottom=$(".player");//底部播放盒区域
							 var  singerspan=playerBottom.find(".singer");//获得歌手展示区
							 singerspan.html(song.author );//更新歌手
							 var snameSpan=playerBottom.find(".sname");//获得歌手展示区
							 snameSpan.html(song.sname);//更新歌名
						}
						if(song.other==0){
							song.other='荐';
						}
						var li='<li>'
						+'<span class="index" >'+song.other+'</span>'
						+'<span class="song">'
						+'	<span class="sname">'+song.sname+'</span><br/>'
						+'	<span class="singer">'+song.author+'</span>'
						+'</span>'
						+'<span class="play">'
						+'	<span class="play2" sname="'+song.sname+'" singer="'+song.author+'" data="'+song.songLink+'">播放</span>'
						+'</span>'
						+'</li>';
						ul.append(li);
					}
				}
			});
		}
	})