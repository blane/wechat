package com.wechat.model;

import java.util.Date;

/**
 *
 * @author xiezc
 * @date 2016年7月16日 下午4:11:01
 */
public class HotSong {

	private int hsId;// 表的id，自增的
	private String sid;// 歌曲的id
	private String author;
	private String sname;
	private String artistld;
	private String lrcLink;
	private String songLink;
	private Date createTime;
	private Integer hotNum;
	private String other;

	public int getHsId() {
		return hsId;
	}

	public void setHsId(int hsId) {
		this.hsId = hsId;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getArtistld() {
		return artistld;
	}

	public void setArtistld(String artistld) {
		this.artistld = artistld;
	}

	public String getLrcLink() {
		return lrcLink;
	}

	public void setLrcLink(String lrcLink) {
		this.lrcLink = lrcLink;
	}

	public String getSongLink() {
		return songLink;
	}

	public void setSongLink(String songLink) {
		this.songLink = songLink;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getHotNum() {
		return hotNum;
	}

	public void setHotNum(Integer hotNum) {
		this.hotNum = hotNum;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

}
