package com.wechat.model;

/**
 *
 * @author xiezc
 * @date 2016年7月16日 下午4:16:40
 */
public class Singer {

	private String singerId;
	private String singerName;
	private String href;
	private int status;

	public String getSingerId() {
		return singerId;
	}

	public void setSingerId(String singerId) {
		this.singerId = singerId;
	}

	public String getSingerName() {
		return singerName;
	}

	public void setSingerName(String singerName) {
		this.singerName = singerName;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
