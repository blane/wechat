package com.wechat.model;

/**
 *
 * @author xiezc
 * @date 2016年7月16日 下午4:06:15
 */
public class Song {

	private int sId;
	private String songId;
	private String singerId;
	private String href;
	private String albumName;
	private String songName;
	private String singerName;
	private int hotNum;
	private String lirc;
	private String file;
	private int index;
	private int songStatus;

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}

	public String getSongId() {
		return songId;
	}

	public void setSongId(String songId) {
		this.songId = songId;
	}

	public String getSingerId() {
		return singerId;
	}

	public void setSingerId(String singerId) {
		this.singerId = singerId;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getSingerName() {
		return singerName;
	}

	public void setSingerName(String singerName) {
		this.singerName = singerName;
	}

	public int getHotNum() {
		return hotNum;
	}

	public void setHotNum(int hotNum) {
		this.hotNum = hotNum;
	}

	public String getLirc() {
		return lirc;
	}

	public void setLirc(String lirc) {
		this.lirc = lirc;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getSongStatus() {
		return songStatus;
	}

	public void setSongStatus(int songStatus) {
		this.songStatus = songStatus;
	}

}
