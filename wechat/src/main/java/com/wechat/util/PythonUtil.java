package com.wechat.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;

import org.apache.log4j.Logger;
import org.xiezc.util.BaseUtil;

/**
 *
 * @author xiezc
 * @date 2016年7月17日 下午2:05:06
 */
public class PythonUtil {
	private static Logger log = Logger.getLogger(PythonUtil.class);

	public static void main(String[] args) {
		getMusic();
		//moveSong();
	}
	
	/**
	 * 同步酷狗音乐文件夹里面的歌曲到项目的python脚本
	 */
	public static void  moveSong(){
		InputStreamReader ir = null;
		OutputStreamWriter osw = null;
		LineNumberReader input = null;
		try {
			BaseUtil b = new BaseUtil();
			String ss = b.getRootPath();
			if (ss.startsWith("/")) {
				ss = ss.substring(1);
			}
			Process process = Runtime.getRuntime()
					.exec(ss+"com/wechat/python/moveSong.bat");
			osw = new OutputStreamWriter(process.getOutputStream(), "GBK");
			osw.write(ss+"com/wechat/python/");
			osw.flush();
			osw.close();
			ir = new InputStreamReader(process.getInputStream(), "GBK");
			input = new LineNumberReader(ir);
			String line;
			while ((line = input.readLine()) != null)
				log.info(line);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
				ir.close();
			} catch (IOException e) {
			}
		}
	}
	
	/**
	 * 运行爬取百度一首歌的方法
	 */
	public static String getOneMusic(String song) {
		OutputStreamWriter osw = null;
		BufferedReader isr = null;
		try {
			BaseUtil b = new BaseUtil();
			String ss = b.getRootPath();
			if (ss.startsWith("/")) {
				ss = ss.substring(1);
			}
			Process process = Runtime.getRuntime()
					.exec(ss+"com/wechat/python/getOne.bat");
			osw = new OutputStreamWriter(process.getOutputStream(), "GBK");
			osw.write(song);
			osw.flush();
			osw.close();
			isr =new BufferedReader(  new InputStreamReader(process.getInputStream(), "GBK"));
			String  line = isr.readLine();
			int index=0;
			String str=null;
			while ((line =isr.readLine())!=null) {
				index++;
				str = new String(line);
				if(index==5){
					return str;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				osw.close();
				isr.close();
			} catch (IOException e) {
			}
		}
		return null;
	}

	/**
	 * 运行爬取百度热歌榜python脚本的方法
	 */
	public static void getMusic() {
		InputStreamReader ir = null;
		LineNumberReader input = null;
		OutputStreamWriter osw=null;
		try {
			BaseUtil b = new BaseUtil();
			String ss = b.getRootPath();
			if (ss.startsWith("/")) {
				ss = ss.substring(1);
			}
			Process process = Runtime.getRuntime()
					.exec(ss+"com/wechat/python/main.bat");
			osw = new OutputStreamWriter(process.getOutputStream(), "GBK");
			osw.write(ss+"com/wechat/python/");
			osw.flush();
			osw.close();
			ir = new InputStreamReader(process.getInputStream(), "GBK");
			input = new LineNumberReader(ir);
			String line;
			while ((line = input.readLine()) != null)
				log.info(line);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				input.close();
				ir.close();
			} catch (IOException e) {
			}
		}
	}
}
