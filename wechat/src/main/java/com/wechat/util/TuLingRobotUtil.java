package com.wechat.util;

import java.util.ArrayList;
import java.util.List;

import org.xiezc.entity.respMsg.Article;
import org.xiezc.entity.respMsg.BaseRespMsg;
import org.xiezc.entity.respMsg.NewsMsg;
import org.xiezc.entity.respMsg.TextMsg;
import org.xiezc.util.BaseUtil;
import org.xiezc.util.MessageFactory;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * 图灵机器人的访问类，将图灵机器人的回复消息转换成微信的回复消息的类
 * 
 * @author xiezc
 * @date 2016年7月16日 下午11:57:02
 */
public class TuLingRobotUtil {

	private static Logger logger=Logger.getLogger(TuLingRobotUtil.class);
	
	/**
	 * 转换方法
	 * 
	 * @param jsonObject
	 *            图灵消息的json格式
	 * @return
	 */
	public static BaseRespMsg convert(JSONObject jsonObject) {
		BaseRespMsg msg = null;

		try {
			int code = jsonObject.getInt("code");
			switch (code) {
			case 100000:
				String text = jsonObject.getString("text");
				msg = MessageFactory.createTextMsg(text);
				break;
			case 200000:
				text = jsonObject.getString("text");
				String url = jsonObject.getString("url");
				msg = ((TextMsg) (MessageFactory.createTextMsg())).addLink(text, url);
				break;
			case 302000:
				text = jsonObject.getString("text");
				List<Article> articles = new ArrayList<>();
				JSONArray list = jsonObject.getJSONArray("list");
				for (int i = 0; i < list.length(); i++) {
					if (i >= 10) {
						break;
					}
					Article article = new Article();
					JSONObject jsonObject2 = list.getJSONObject(i);
					article.setTitle(jsonObject2.getString("article"));
					article.setDescription("新闻来自:" + jsonObject2.getString("source"));
					article.setPicUrl(jsonObject2.getString("icon"));
					article.setUrl(jsonObject2.getString("detailurl"));
					articles.add(article);
				}
				NewsMsg newsMsg = MessageFactory.createNewsMsg();
				msg = newsMsg.setArticles(articles);
				break;
			case 308000:
				text = jsonObject.getString("text");
				list = jsonObject.getJSONArray("list");
				articles = new ArrayList<>();
				for (int i = 0; i < list.length(); i++) {
					if (i >= 10) {
						break;
					}
					Article article = new Article();
					JSONObject jsonObject2 = list.getJSONObject(i);
					article.setTitle(jsonObject2.getString("name"));
					article.setDescription(jsonObject2.getString("info"));
					article.setPicUrl(jsonObject2.getString("icon"));
					article.setUrl(jsonObject2.getString("detailurl"));
					articles.add(article);
				}
				newsMsg = MessageFactory.createNewsMsg();
				msg = newsMsg.setArticles(articles);
				break;
			case 40002:
				text = jsonObject.getString("text");
				msg = MessageFactory.createTextMsg(text);
				break;
			default:
				msg = MessageFactory.createTextMsg("亲，不明白你在说什么。");
				break;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return msg;
	}

	private final static String apikey = "2c06247db9abfcd161d1b6a79ffd7a3f";
	private final static String url = "http://www.tuling123.com/openapi/api";
	@SuppressWarnings("unused")
	private final static String secret = "3b3969d39f710611";

	public static BaseRespMsg talkWithTuLing(String content, String openId) {
		try {
			// delete space
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("key", apikey);
			jsonObject.put("userid", openId);
			jsonObject.put("info", content.trim());
			String result = BaseUtil.sendPost(url, jsonObject.toString());
			logger.debug("图灵返回时"+result);
			JSONObject jsonObject2 = new JSONObject(result);
			return convert(jsonObject2);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;

	}

}
