package com.wechat.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wechat.model.User;

/**
 *
 * @author xiezc
 * @date 2016年7月16日 下午4:31:22
 */
public interface UserMapper {

	public List<User> getUsers(@Param("start")int start,@Param("size") int size);
	
	public User getUser(User user);
	
	public void updateUser(User user);
	
	public void insertUser(User user);
	
	public void deleteUser(String openId);
	
	
}
