package com.wechat.dao;

import java.util.List;


import org.apache.ibatis.annotations.Param;

import com.wechat.model.HotSong;

/**
 *
 * @author xiezc
 * @date 2016年7月16日 下午6:26:53
 */
public interface HotSongMapper {

	public void add(HotSong hotSong);

	public List<HotSong> getHotSongs(@Param("singerName") String singerName,@Param("songName") String songName);

	public void update(HotSong hotSong);
	
	public List<HotSong> getHotSongsOrderBy(int size);
	
	public List<HotSong> getMusicOnePage(@Param("sname") String sname,@Param("start") int start,@Param("pageSize") int pageSize);
	

}
