package com.wechat.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;


import com.wechat.model.Song;

/**
 *
 * @author xiezc
 * @date 2016年7月17日 下午8:11:32
 */
public interface SongMapper {

	public List<Song> getSong(@Param("author")String author,@Param("songName")String sname);

}
