package com.wechat.service;

import java.util.Map;

import org.xiezc.entity.reqMsg.event.EventType;
import org.xiezc.entity.reqMsg.event.LocationEvent;
import org.xiezc.entity.reqMsg.event.MenuEvent;
import org.xiezc.entity.reqMsg.event.QrCodeEvent;
import org.xiezc.entity.reqMsg.event.SubscribeEvent;
import org.xiezc.entity.reqMsg.msg.ImageReqMsg;
import org.xiezc.entity.reqMsg.msg.LinkReqMsg;
import org.xiezc.entity.reqMsg.msg.LocationReqMsg;
import org.xiezc.entity.reqMsg.msg.MsgType;
import org.xiezc.entity.reqMsg.msg.ShortVideoReqMsg;
import org.xiezc.entity.reqMsg.msg.TextReqMsg;
import org.xiezc.entity.reqMsg.msg.VideoReqMsg;
import org.xiezc.entity.reqMsg.msg.VoiceReqMsg;
import org.xiezc.entity.respMsg.BaseRespMsg;

/**
 * 处理消息的接口
 * 
 * @author xiezc
 *
 */
public class HandleMsgService {

	protected BaseRespMsg handleMenuEvent(MenuEvent menuEvent) {
		// TODO Auto-generated method stub
		return null;
	}

	protected BaseRespMsg handleLocationEvent(LocationEvent locationEvent) {
		// TODO Auto-generated method stub
		return null;
	}

	protected BaseRespMsg handleQrCodeEvent(QrCodeEvent qrCodeEvent) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 处理用户的关注和取消关注事件
	 * 
	 * @param subscribeEvent
	 * @return
	 */
	protected BaseRespMsg handleSubscribeEvent(SubscribeEvent subscribeEvent) {
		// TODO Auto-generated method stub
		return null;
	}

	protected BaseRespMsg handleLocationReqMsg(LocationReqMsg locationReqMsg) {
		// TODO Auto-generated method stub
		return null;
	}

	protected BaseRespMsg handleShortVideoReqMsg(ShortVideoReqMsg shortVideoReqMsg) {
		// TODO Auto-generated method stub
		return null;
	}

	protected BaseRespMsg handleVideoReqMsg(VideoReqMsg videoReqMsg) {
		// TODO Auto-generated method stub
		return null;
	}

	protected BaseRespMsg handleVoiceReqMsg(VoiceReqMsg voiceReqMsg) {
		// TODO Auto-generated method stub
		return null;
	}

	protected BaseRespMsg handleImagereqMsg(ImageReqMsg imageReqMsg) {
		// TODO Auto-generated method stub
		return null;
	}

	protected BaseRespMsg handleLinkReqMsg(LinkReqMsg linkReqMsg) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 处理用户的文本消息
	 * 
	 * @param textReqMsg
	 * @return
	 */
	protected BaseRespMsg handleTextReqMsg(TextReqMsg textReqMsg) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 此类主要根据请求的消息类型来分发函数处理消息（适配器模式）
	 * 
	 * @param map
	 *            装载了用户发送的消息的map
	 * @return 返回回复的消息
	 */
	public BaseRespMsg handleMsg(Map<String, String> map) {
		BaseRespMsg baseRespMsg = null;
		String msgType = map.get("MsgType");
		Long createTime = Long.parseLong(map.get("CreateTime"));
		String fromUserName = map.get("FromUserName");
		String toUserName = map.get("ToUserName");
		String msgId = map.get("MsgId");
		// 发来的是事件
		if (MsgType.EVENT.equals(msgType)) {
			String event = map.get("Event");
			switch (event) {
			case EventType.SUBSCRIBE:// 订阅事件
				String ticket = map.get("Ticket");
				if (ticket != null) {// 扫描带参数二维码事件, 用户未关注时，进行关注后的事件推送
					String eventKey = map.get("EventKey");
					QrCodeEvent qrCodeEvent = new QrCodeEvent(eventKey, ticket, EventType.SUBSCRIBE);
					qrCodeEvent.setCreateTime(createTime);
					qrCodeEvent.setFromUserName(fromUserName);
					qrCodeEvent.setToUserName(toUserName);
					baseRespMsg = handleQrCodeEvent(qrCodeEvent);
					break;
				}
				// 订阅事件
				SubscribeEvent subscribeEvent = new SubscribeEvent(event);
				subscribeEvent.setCreateTime(createTime);
				subscribeEvent.setFromUserName(fromUserName);
				subscribeEvent.setToUserName(toUserName);
				baseRespMsg = handleSubscribeEvent(subscribeEvent);
				break;
			case EventType.UNSUBSCRIBE:// 取消订阅事件
				subscribeEvent = new SubscribeEvent(EventType.UNSUBSCRIBE);
				subscribeEvent.setCreateTime(createTime);
				subscribeEvent.setFromUserName(fromUserName);
				subscribeEvent.setToUserName(toUserName);
				baseRespMsg = handleSubscribeEvent(subscribeEvent);
				break;
			case EventType.SCAN:// 扫描带参数二维码事件, 用户已关注时的事件推送
				ticket = map.get("Ticket");
				String eventKey = map.get("EventKey");
				QrCodeEvent qrCodeEvent = new QrCodeEvent(eventKey, ticket, EventType.SCAN);
				qrCodeEvent.setCreateTime(createTime);
				qrCodeEvent.setFromUserName(fromUserName);
				qrCodeEvent.setToUserName(toUserName);
				baseRespMsg = handleQrCodeEvent(qrCodeEvent);
				break;
			case EventType.LOCATION:// 上报地理位置事件
				// Latitude 地理位置纬度
				// Longitude 地理位置经度
				// Precision 地理位置精度
				String latitude = map.get("Latitude");
				String Longitude = map.get("Longitude");
				String precision = map.get("Precision");
				LocationEvent locationEvent = new LocationEvent(Double.parseDouble(latitude),
						Double.parseDouble(Longitude), Double.parseDouble(precision));
				locationEvent.setCreateTime(createTime);
				locationEvent.setFromUserName(fromUserName);
				locationEvent.setToUserName(toUserName);
				baseRespMsg = handleLocationEvent(locationEvent);
				break;
			case EventType.CLICK:// 自定义菜单事件,点击菜单拉取消息时的事件推送
				// EventKey 事件KEY值，与自定义菜单接口中KEY值对应
				eventKey = map.get("EventKey");
				MenuEvent menuEvent = new MenuEvent(eventKey, EventType.CLICK);
				menuEvent.setCreateTime(createTime);
				menuEvent.setFromUserName(fromUserName);
				menuEvent.setToUserName(toUserName);
				baseRespMsg = handleMenuEvent(menuEvent);
				break;
			case EventType.VIEW:// 自定义菜单事件. 点击菜单跳转链接时的事件推送
				// EventKey 事件KEY值，与自定义菜单接口中KEY值对应
				eventKey = map.get("EventKey");
				menuEvent = new MenuEvent(eventKey, EventType.VIEW);
				menuEvent.setCreateTime(createTime);
				menuEvent.setFromUserName(fromUserName);
				menuEvent.setToUserName(toUserName);

				baseRespMsg = handleMenuEvent(menuEvent);
				break;
			default:
				break;
			}

		} else {

			// 说明发来的是消息，不是事件
			switch (msgType) {
			case MsgType.TEXT:// 文本消息
				String content = map.get("Content");
				TextReqMsg textReqMsg = new TextReqMsg(content);
				textReqMsg.setCreateTime(createTime);
				textReqMsg.setFromUserName(fromUserName);
				textReqMsg.setToUserName(toUserName);
				textReqMsg.setMsgId(msgId);
				baseRespMsg = handleTextReqMsg(textReqMsg);
				break;
			case MsgType.LINK:// 链接消息

				String title = map.get("Title");
				String description = map.get("Description");
				String url = map.get("Url");
				LinkReqMsg linkReqMsg = new LinkReqMsg(title, description, url);
				linkReqMsg.setCreateTime(createTime);
				linkReqMsg.setFromUserName(fromUserName);
				linkReqMsg.setToUserName(toUserName);
				linkReqMsg.setMsgId(msgId);
				baseRespMsg = handleLinkReqMsg(linkReqMsg);
				break;
			case MsgType.IMAGE:// 图片消息
				String picUrl = map.get("PicUrl");
				String mediaId = map.get("MediaId");
				ImageReqMsg imageReqMsg = new ImageReqMsg(picUrl, mediaId);
				imageReqMsg.setCreateTime(createTime);
				imageReqMsg.setFromUserName(fromUserName);
				imageReqMsg.setToUserName(toUserName);
				imageReqMsg.setMsgId(msgId);
				baseRespMsg = handleImagereqMsg(imageReqMsg);
				break;
			case MsgType.VOICE:// 语音消息
				// MediaId 语音消息媒体id，可以调用多媒体文件下载接口拉取数据。
				// Format 语音格式，如amr，speex等
				mediaId = null;
				mediaId = map.get("MediaId");
				String format = map.get("Format");
				String recognition = map.get("Recognition");
				VoiceReqMsg voiceReqMsg = new VoiceReqMsg(mediaId, format, recognition);
				voiceReqMsg.setCreateTime(createTime);
				voiceReqMsg.setFromUserName(fromUserName);
				voiceReqMsg.setToUserName(toUserName);
				voiceReqMsg.setMsgId(msgId);
				baseRespMsg = handleVoiceReqMsg(voiceReqMsg);
				break;
			case MsgType.VIDEO:// 视频消息
				// MediaId 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
				// ThumbMediaId
				mediaId = null;
				mediaId = map.get("MediaId");
				String thumbMediaId = map.get("ThumbMediaId");
				VideoReqMsg videoReqMsg = new VideoReqMsg(mediaId, thumbMediaId);
				videoReqMsg.setCreateTime(createTime);
				videoReqMsg.setFromUserName(fromUserName);
				videoReqMsg.setToUserName(toUserName);
				videoReqMsg.setMsgId(msgId);
				baseRespMsg = handleVideoReqMsg(videoReqMsg);
				break;
			case MsgType.SHORTVIDEO:// 小视频消息
				// MediaId 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
				// ThumbMediaId
				mediaId = null;
				mediaId = map.get("MediaId");
				thumbMediaId = map.get("ThumbMediaId");
				ShortVideoReqMsg shortVideoReqMsg = new ShortVideoReqMsg(mediaId, thumbMediaId);

				shortVideoReqMsg.setCreateTime(createTime);
				shortVideoReqMsg.setFromUserName(fromUserName);
				shortVideoReqMsg.setToUserName(toUserName);
				shortVideoReqMsg.setMsgId(msgId);
				baseRespMsg = handleShortVideoReqMsg(shortVideoReqMsg);
				break;
			case MsgType.LOCATION:// 位置消息
				// Location_X 地理位置维度
				// Location_Y 地理位置经度
				// Scale 地图缩放大小
				// Label 地理位置信息
				String location_X = map.get("Location_X");
				String location_Y = map.get("Location_Y");
				String scale = map.get("Scale");
				String label = map.get("Label");
				LocationReqMsg locationReqMsg = new LocationReqMsg(Double.parseDouble(location_X),
						Double.parseDouble(location_Y), Integer.parseInt(scale), label);

				locationReqMsg.setCreateTime(createTime);
				locationReqMsg.setFromUserName(fromUserName);
				locationReqMsg.setToUserName(toUserName);
				locationReqMsg.setMsgId(msgId);

				baseRespMsg = handleLocationReqMsg(locationReqMsg);
				break;
			default:
				break;
			}

		}
		return baseRespMsg;
	}
}
