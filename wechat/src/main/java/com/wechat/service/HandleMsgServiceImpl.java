package com.wechat.service;

import java.net.URLEncoder;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.xiezc.InitializeListerner;
import org.xiezc.entity.reqMsg.event.LocationEvent;
import org.xiezc.entity.reqMsg.event.MenuEvent;
import org.xiezc.entity.reqMsg.event.QrCodeEvent;
import org.xiezc.entity.reqMsg.event.SubscribeEvent;
import org.xiezc.entity.reqMsg.msg.ImageReqMsg;
import org.xiezc.entity.reqMsg.msg.LinkReqMsg;
import org.xiezc.entity.reqMsg.msg.LocationReqMsg;
import org.xiezc.entity.reqMsg.msg.ShortVideoReqMsg;
import org.xiezc.entity.reqMsg.msg.TextReqMsg;
import org.xiezc.entity.reqMsg.msg.VideoReqMsg;
import org.xiezc.entity.reqMsg.msg.VoiceReqMsg;
import org.xiezc.entity.respMsg.BaseRespMsg;
import org.xiezc.entity.respMsg.MusicMsg;
import org.xiezc.entity.respMsg.TextMsg;
import org.xiezc.util.MessageFactory;
import org.xiezc.util.StrUtil;

import com.wechat.dao.HotSongMapper;
import com.wechat.dao.SongMapper;
import com.wechat.model.HotSong;
import com.wechat.util.TuLingRobotUtil;

/**
 * 出收到的具体消息的具体业务类
 * 
 * @author xiezc
 * @date 2016年7月16日 下午11:49:48
 */
@Service("handleMsgService")
public class HandleMsgServiceImpl extends HandleMsgService {

	private static Logger logger=Logger.getLogger(HandleMsgServiceImpl.class); 
	@Resource
	private HotSongMapper hotSongMapper;
	@Resource
	private SongMapper songMapper;

	@Override
	protected BaseRespMsg handleSubscribeEvent(SubscribeEvent subscribeEvent) {
		// 获得用户的事件名
		String event = subscribeEvent.getEvent();
		TextMsg textMsg = null;
		if ("subscribe".equals(event)) {// 关注
			textMsg = MessageFactory.createTextMsg("感谢关注又最帅的xiezc开发的公众号，发送：听歌     {歌名}， 可以听歌");
		}
		return textMsg;
	}

	@Override
	protected BaseRespMsg handleTextReqMsg(TextReqMsg textReqMsg) {
		String content = textReqMsg.getContent().trim();
		if (content.startsWith("?听歌") || content.startsWith("？听歌") || content.startsWith("听歌")) {
			String mc = content.replace("听歌", "").replace("听歌", "").replace("？", "").replace("?", "").trim();
			String singer = null;
			String songName = null;
			if (mc.contains("-")) {
				singer = mc.split("-")[0].trim();
				songName = mc.split("-")[1].trim();
			} else {
				songName = mc;
			}
			return getSong(singer, songName);
		}
		if (content.startsWith("?歌榜") || content.startsWith("？歌榜") || content.startsWith("排行榜")) {
			List<HotSong> list = hotSongMapper.getMusicOnePage(null, 0, 10);
			TextMsg textMsg = MessageFactory.createTextMsg();
			for (HotSong hotSong : list) {
				String musicUrl = InitializeListerner.wechatConfig.getUrl() + "/rank.html";
				textMsg.addLink(hotSong.getAuthor() + "-" + hotSong.getSname(), musicUrl);
				textMsg.addln();
			}
			return textMsg;
		}
		BaseRespMsg baseRespMsg = TuLingRobotUtil.talkWithTuLing(content, textReqMsg.getFromUserName());
		return baseRespMsg;
	}

	/**
	 * 根据用户的输入查询下载歌曲
	 * 
	 * @param content
	 * @return
	 */
	public BaseRespMsg getSong(String singer, String songName) {
		try {
			// 用户输入听歌则随机获得一首歌给用户
			if (StrUtil.isNOB(songName) && StrUtil.isNOB(singer)) {// 热歌榜没有
				int random = (int) (Math.random() * 500);
				//获得随机的歌曲
				List<HotSong> list1 = hotSongMapper.getMusicOnePage(null, random, 1);
				logger.debug("list1 is Empty:"+list1.isEmpty());
				HotSong hotSong = list1.get(0);
				songName = URLEncoder.encode(hotSong.getSongLink().trim(), "utf-8");
				String musicUrl = InitializeListerner.wechatConfig.getUrl() + "/music?songName=" + songName;
				MusicMsg musicMsg = MessageFactory.createMusicMsg("", hotSong.getSname().trim(),
						"歌手：" + hotSong.getAuthor(), musicUrl, musicUrl);
				return musicMsg;
			}

			List<HotSong> list = hotSongMapper.getHotSongs(singer, songName);
			if (list.isEmpty()) {// 热歌榜没有
				List<HotSong> list1 = hotSongMapper.getMusicOnePage(null, 0, 10);
				TextMsg textMsg = MessageFactory.createTextMsg("没找到你的歌，排行榜前十首歌如下");
				textMsg.addln();
				for (HotSong hotSong : list1) {
					String musicUrl = InitializeListerner.wechatConfig.getUrl() + "/rank.html";
					textMsg.addLink(hotSong.getAuthor() + "-" + hotSong.getSname(), musicUrl);
					textMsg.addln();
				}
				return textMsg;
			}
			HotSong hotSong = list.get(0);
			songName = URLEncoder.encode(hotSong.getSongLink().trim(), "utf-8");
			String musicUrl = InitializeListerner.wechatConfig.getUrl() + "/music?songName=" + songName;
			MusicMsg musicMsg = MessageFactory.createMusicMsg("", hotSong.getSname().trim(),
					"歌手：" + hotSong.getAuthor(), musicUrl, musicUrl);
			return musicMsg;
		} catch (Exception e) {
			e.printStackTrace();
			TextMsg textMsg = MessageFactory.createTextMsg("");
			textMsg.addLink("没找到你的歌，点击查看歌曲排行榜", "http://xiezc.vip.natapp.cn/rank.html");
			return textMsg;
		}
	}

	@Override
	protected BaseRespMsg handleImagereqMsg(ImageReqMsg imageReqMsg) {
		// TODO Auto-generated method stub
		return super.handleImagereqMsg(imageReqMsg);
	}

	@Override
	protected BaseRespMsg handleLinkReqMsg(LinkReqMsg linkReqMsg) {
		// TODO Auto-generated method stub
		return super.handleLinkReqMsg(linkReqMsg);
	}

	@Override
	protected BaseRespMsg handleLocationEvent(LocationEvent locationEvent) {
		// TODO Auto-generated method stub
		return super.handleLocationEvent(locationEvent);
	}

	@Override
	protected BaseRespMsg handleLocationReqMsg(LocationReqMsg locationReqMsg) {
		// TODO Auto-generated method stub
		return super.handleLocationReqMsg(locationReqMsg);
	}

	@Override
	protected BaseRespMsg handleMenuEvent(MenuEvent menuEvent) {
		// TODO Auto-generated method stub
		return super.handleMenuEvent(menuEvent);
	}

	@Override
	protected BaseRespMsg handleQrCodeEvent(QrCodeEvent qrCodeEvent) {
		// TODO Auto-generated method stub
		return super.handleQrCodeEvent(qrCodeEvent);
	}

	@Override
	protected BaseRespMsg handleShortVideoReqMsg(ShortVideoReqMsg shortVideoReqMsg) {
		// TODO Auto-generated method stub
		return super.handleShortVideoReqMsg(shortVideoReqMsg);
	}

	@Override
	protected BaseRespMsg handleVideoReqMsg(VideoReqMsg videoReqMsg) {
		// TODO Auto-generated method stub
		return super.handleVideoReqMsg(videoReqMsg);
	}

	@Override
	protected BaseRespMsg handleVoiceReqMsg(VoiceReqMsg voiceReqMsg) {
		// TODO Auto-generated method stub
		return super.handleVoiceReqMsg(voiceReqMsg);
	}

}
