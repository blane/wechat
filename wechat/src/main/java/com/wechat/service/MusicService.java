package com.wechat.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.wechat.dao.HotSongMapper;
import com.wechat.model.HotSong;

/**
 *
 * @author xiezc
 * @date 2016年7月22日 下午9:30:35
 */
@Service("musicService")
public class MusicService {

	@Resource
	private HotSongMapper hotSongMapper;

	/**
	 * 获得一页的歌曲
	 * @param sname
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<HotSong> getMusics(String sname, int pageNo, int pageSize) {
		int start = (pageNo - 1) * pageSize;
		List<HotSong> list = hotSongMapper.getMusicOnePage(sname, start, pageSize);
		return list;
	}

}
