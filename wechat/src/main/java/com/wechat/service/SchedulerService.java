package com.wechat.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.wechat.util.PythonUtil;

/**
 *
 * @author xiezc
 * @date 2016年7月17日 下午3:28:30
 */
@Service
public class SchedulerService {

	/**
	 * 定时任务的获得百度热歌榜的歌曲
	 */
	@Scheduled(cron = "0 00 21 * * ?") // 每晚九点开始执行
	public void downloadHotSong() {
		try {
			//爬取百度热歌榜的脚本执行
			PythonUtil.getMusic();
			//移动酷狗music的脚本执行
			PythonUtil.moveSong();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
