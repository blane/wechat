package com.wechat.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.xiezc.servlet.MyHttpServlet;

/**
 *
 * @author xiezc
 * @date 2016年7月19日 下午10:16:02
 */
@WebServlet("*.html")
public class RankServlet extends MyHttpServlet {
	private static final long serialVersionUID = 1L;

	private final String rootPath = "/WEB-INF/view/";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String returnUrl = rootPath + "404.jsp";
		String url = req.getRequestURI();
		if (url.length() > 5) {
			returnUrl = rootPath + url.substring(0, url.length() - 5) + ".jsp";
		}
		req.getRequestDispatcher(returnUrl).forward(req, resp);
		return;
	}


}
