package org.xiezc.entity.respMsg;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.xiezc.util.MessageBuilder;

/**
 * 回复图文消息
 * 
 * <xml> 
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>12345678</CreateTime> 
 * <MsgType><![CDATA[news]]></MsgType>
 * <ArticleCount>2</ArticleCount>
 * <Articles> 
 * 		<item> 
 * 			<Title><![CDATA[title1]]></Title>
 * 			<Description><![CDATA[description1]]></Description>
 * 			<PicUrl><![CDATA[picurl]]></PicUrl> 
 * 			<Url><![CDATA[url]]></Url> 
 * 		</item>
 * 		<item> 
 * 			<Title><![CDATA[title]]></Title>
 * 			<Description><![CDATA[description]]></Description>
 * 			<PicUrl><![CDATA[picurl]]></PicUrl>
 * 			<Url><![CDATA[url]]></Url> 
 *		</item> 
 * </Articles> 
 * </xml>
 * 
 * 参数 是否必须 说明<br>
 * ToUserName 是 接收方帐号（收到的OpenID）<br>
 * FromUserName 是 开发者微信号<br>
 * CreateTime 是 消息创建时间 （整型）<br>
 * MsgType 是 news<br>
 * ArticleCount 是 图文消息个数，限制为10条以内<br>
 * Articles 是 多条图文消息信息，默认第一个item为大图,注意，如果图文数超过10，则将会无响应<br>
 * Title 否 图文消息标题<br>
 * Description 否 图文消息描述<br>
 * PicUrl 否 图片链接，支持JPG、PNG格式，较好的效果为大图360*200，小图200*200<br>
 * Url 否 点击图文消息跳转链接<br>
 * 
 * @author xiezc
 *
 */
public class NewsMsg extends BaseRespMsg {
	private static Logger logger = Logger.getLogger(NewsMsg.class);

	private static final int WX_MAX_SIZE = 10;
	private int maxSize = WX_MAX_SIZE;

	List<Article> articles;

	public NewsMsg() {
		this.articles = new ArrayList<Article>(maxSize);
	}

	public NewsMsg(int maxSize) {
		setMaxSize(maxSize);
		this.articles = new ArrayList<Article>(maxSize);
	}

	public NewsMsg(List<Article> articles) {
		setArticles(articles);
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		if (maxSize > WX_MAX_SIZE || maxSize < 1) {
			maxSize = WX_MAX_SIZE;
		}
		this.maxSize = maxSize;
		if (articles != null && articles.size() > maxSize) {
			articles = articles.subList(0, maxSize);
		}
	}

	public List<Article> getArticles() {
		return articles;
	}

	public NewsMsg setArticles(List<Article> articles) {
		if (articles.size() > maxSize) {
			this.articles = articles.subList(0, maxSize);
		} else {
			this.articles = articles;
		}
		return this;
	}

	public NewsMsg add(String title) {
		return add(title, null, null, null);
	}

	public NewsMsg add(String title, String url) {
		return add(title, null, null, url);
	}

	public NewsMsg add(String title, String picUrl, String url) {
		return add(new Article(title, null, picUrl, url));
	}

	public NewsMsg add(String title, String description, String picUrl, String url) {
		return add(new Article(title, description, picUrl, url));
	}

	public NewsMsg add(Article article) {
		if (this.articles.size() < maxSize) {
			this.articles.add(article);
		} else {
			logger.warn("Article条数超出限制");
			
		}
		return this;
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", RespType.NEWS);
		mb.addTag("ArticleCount", String.valueOf(articles.size()));
		mb.append("<Articles>\n");
		for (Article article : articles) {
			mb.append(article.toXml());
		}
		mb.append("</Articles>\n");
		mb.surroundWith("xml");
		return mb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("NewsMsg [articles=\n");
		for (int i = 0; i < articles.size(); i++) {
			sb.append("  Article ").append(i).append(": ").append(articles.get(i)).append("\n");
		}
		sb.append("]");
		return sb.toString();
	}

}
