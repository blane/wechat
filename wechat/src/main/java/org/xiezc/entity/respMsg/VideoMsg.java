package org.xiezc.entity.respMsg;

import org.xiezc.util.MessageBuilder;

/**
 * 回复视频消息
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>12345678</CreateTime> 
 * <MsgType><![CDATA[video]]></MsgType>
 * <Video> 
 * 		<MediaId><![CDATA[media_id]]></MediaId>
 * 		<Title><![CDATA[title]]></Title>
 * 		<Description><![CDATA[description]]></Description> 
 * </Video> 
 * </xml>
 * 
 * 参数 是否必须 说明<br>
 * ToUserName 是 接收方帐号（收到的OpenID）<br>
 * FromUserName 是 开发者微信号<br>
 * CreateTime 是 消息创建时间 （整型）<br>
 * MsgType 是 video<br>
 * MediaId 是 通过素材管理接口上传多媒体文件，得到的id<br>
 * Title 否 视频消息的标题<br>
 * Description 否 视频消息的描述<br>
 */
public class VideoMsg extends BaseRespMsg {

	private String mediaId;
	private String title;
	private String description;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public VideoMsg(String mediaId) {
		this.mediaId = mediaId;
	}

	public VideoMsg(String mediaId, String title, String description) {
		this.mediaId = mediaId;
		this.title = title;
		this.description = description;
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", RespType.VIDEO);
		mb.append("<Video>\n");
		mb.addData("MediaId", mediaId);
		mb.addData("Title", title);
		mb.addData("Description", description);
		mb.append("</Video>\n");
		mb.surroundWith("xml");

		return mb.toString();
	}

	@Override
	public String toString() {
		return "VideoMsg [mediaId=" + mediaId + ", title=" + title + ", description=" + description + "]";
	}

}
