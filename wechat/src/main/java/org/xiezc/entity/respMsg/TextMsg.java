package org.xiezc.entity.respMsg;

import org.xiezc.util.MessageBuilder;

/**
 * 回复文本消息
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>12345678</CreateTime> 
 * <MsgType><![CDATA[text]]></MsgType>
 * <Content><![CDATA[你好]]></Content> </xml>
 * 
 * 参数 是否必须 描述 <br>
 * ToUserName 是 接收方帐号（收到的OpenID）<br>
 * FromUserName 是 开发者微信号<br>
 * CreateTime 是 消息创建时间 （整型）<br>
 * MsgType 是 text<br>
 * Content 是 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示）<br>
 * 
 * @author xiezc
 *
 */
public final class TextMsg extends BaseRespMsg {

	private StringBuilder contentBuilder;

	public String getContent() {
		return contentBuilder.toString();
	}

	public void setContent(String content) {
		contentBuilder = new StringBuilder(content);
	}

	public TextMsg() {
		super();
		this.setMsgType(RespType.TEXT);
		contentBuilder = new StringBuilder();
	}

	public TextMsg(String content) {
		super();
		this.setMsgType(RespType.TEXT);
		setContent(content);
	}

	public TextMsg add(String text) {
		contentBuilder.append(text);
		return this;
	}

	public TextMsg addln() {
		return add("\n");
	}

	public TextMsg addln(String text) {
		contentBuilder.append(text);
		return addln();
	}

	public TextMsg addLink(String text, String url) {
		contentBuilder.append("<a href=\"").append(url).append("\">").append(text).append("</a>");
		return this;
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("Content", contentBuilder.toString());
		mb.addData("MsgType", RespType.TEXT);
		mb.surroundWith("xml");

		return mb.toString();
	}

	@Override
	public String toString() {
		return "TextMsg [content=" + getContent() + "]";
	}

}
