package org.xiezc.entity.respMsg;

import org.xiezc.util.MessageBuilder;

/**
 * 回复语音消息
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>12345678</CreateTime> <MsgType><![CDATA[voice]]></MsgType>
 * <Voice> <MediaId><![CDATA[media_id]]></MediaId> </Voice> </xml>
 * 
 * 参数 是否必须 说明<br>
 * ToUserName 是 接收方帐号（收到的OpenID）<br>
 * FromUserName 是 开发者微信号<br>
 * CreateTime 是 消息创建时间戳 （整型）<br>
 * MsgType 是 语音，voice<br>
 * MediaId 是 通过素材管理接口上传多媒体文件，得到的id<br>
 * 
 * @author xiezc
 *
 */
public class VoiceMsg extends BaseRespMsg {

	private String mediaId;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public VoiceMsg(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", RespType.VOICE);
		mb.append("<Voice>");
		mb.addData("MediaId", mediaId);
		mb.append("</Voice>");
		mb.surroundWith("xml");

		return mb.toString();
	}

	@Override
	public String toString() {
		return "VoiceMsg [mediaId=" + mediaId + "]";
	}

}
