package org.xiezc.entity.respMsg;

import org.xiezc.util.MessageBuilder;
import org.xiezc.util.StrUtil;

/**
 * 回复音乐消息
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>12345678</CreateTime> <MsgType><![CDATA[music]]></MsgType>
 * <Music> <Title><![CDATA[TITLE]]></Title>
 * <Description><![CDATA[DESCRIPTION]]></Description>
 * <MusicUrl><![CDATA[MUSIC_Url]]></MusicUrl>
 * <HQMusicUrl><![CDATA[HQ_MUSIC_Url]]></HQMusicUrl>
 * <ThumbMediaId><![CDATA[media_id]]></ThumbMediaId> </Music> </xml>
 * 
 * 参数 是否必须 说明<br>
 * ToUserName 是 接收方帐号（收到的OpenID）<br>
 * FromUserName 是 开发者微信号<br>
 * CreateTime 是 消息创建时间 （整型）<br>
 * MsgType 是 music<br>
 * Title 否 音乐标题<br>
 * Description 否 音乐描述<br>
 * MusicURL 否 音乐链接<br>
 * HQMusicUrl 否 高质量音乐链接，WIFI环境优先使用该链接播放音乐<br>
 * ThumbMediaId 否 缩略图的媒体id，通过素材管理接口上传多媒体文件，得到的id<br>
 * 
 * @author xiezc
 *
 */
public class MusicMsg extends BaseRespMsg {

	private String title;
	private String description;
	private String musicUrl;
	private String hqMusicUrl;
	private String thumbMediaId;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMusicUrl() {
		return musicUrl;
	}

	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}

	public String getHqMusicUrl() {
		return hqMusicUrl;
	}

	public void setHqMusicUrl(String hqMusicUrl) {
		this.hqMusicUrl = hqMusicUrl;
	}

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public MusicMsg(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public MusicMsg(String thumbMediaId, String title, String description, String musicUrl, String hqMusicUrl) {
		this.title = title;
		this.description = description;
		this.musicUrl = musicUrl;
		this.hqMusicUrl = hqMusicUrl;
		this.thumbMediaId = thumbMediaId;
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", RespType.MUSIC);
		mb.append("<Music>\n");
		mb.addData("Title", title);
		mb.addData("Description", description);
		mb.addData("MusicUrl", musicUrl);
		mb.addData("HQMusicUrl", hqMusicUrl);
		if (!StrUtil.isNOB(thumbMediaId)) {
			mb.addData("ThumbMediaId", thumbMediaId);
		}
		mb.append("</Music>\n");
		mb.surroundWith("xml");

		return mb.toString();
	}

	@Override
	public String toString() {
		return "MusicMsg [title=" + title + ", description=" + description + ", musicUrl=" + musicUrl + ", hqMusicUrl="
				+ hqMusicUrl + ", thumbMediaId=" + thumbMediaId + "]";
	}

}
