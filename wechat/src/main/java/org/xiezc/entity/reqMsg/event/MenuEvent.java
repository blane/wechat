package org.xiezc.entity.reqMsg.event;

import org.xiezc.util.MessageBuilder;

/**
 * 自定义菜单事件
 * 
 * 用户点击自定义菜单后，微信会把点击事件推送给开发者，请注意，点击菜单弹出子菜单，不会产生上报。
 * 
 * 点击菜单拉取消息时的事件推送
 * 
 * 推送XML数据包示例：
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[CLICK]]></Event>
 * <EventKey><![CDATA[EVENTKEY]]></EventKey> </xml>
 * 
 * 参数说明：
 * 
 * 参数 描述<br>
 * ToUserName 开发者微信号<br>
 * FromUserName 发送方帐号（一个OpenID）<br>
 * CreateTime 消息创建时间 （整型）<br>
 * MsgType 消息类型，event<br>
 * Event 事件类型，CLICK<br>
 * EventKey 事件KEY值，与自定义菜单接口中KEY值对应<br>
 * 
 * 点击菜单跳转链接时的事件推送
 * 
 * 推送XML数据包示例：
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[VIEW]]></Event>
 * <EventKey><![CDATA[www.qq.com]]></EventKey> </xml> 参数说明：
 * 
 * 参数 描述 ToUserName 开发者微信号<br>
 * FromUserName 发送方帐号（一个OpenID）<br>
 * CreateTime 消息创建时间 （整型）<br>
 * MsgType 消息类型，event<br>
 * Event 事件类型，VIEW<br>
 * EventKey 事件KEY值，设置的跳转URL<br>
 * 
 * @author xiezc
 *
 */
public final class MenuEvent extends BaseEvent {

	private String eventKey;

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public String getEventKey() {
		return eventKey;
	}

	@Override
	public String getEvent() {
		return super.getEvent();
	}

	public MenuEvent(String eventKey, String event) {
		super();
		this.eventKey = eventKey;
		if (EventType.CLICK.equals(event) || EventType.VIEW.equals(event)) {
			setEvent(event);
		} else {
			throw new RuntimeException("传入的事件参数类型不对");
		}

	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toString());
		mb.addData("EventKey", eventKey);
		mb.surroundWith("xml");
		return mb.toString();
	}

}
