package org.xiezc.entity.reqMsg.event;

import org.xiezc.util.MessageBuilder;

/**
 * 上报地理位置事件
 * 
 * 用户同意上报地理位置后，每次进入公众号会话时，都会在进入时上报地理位置，或在进入会话后每5秒上报一次地理位置，公众号可以在公众平台网站中修改以上设置。
 * 上报地理位置时，微信会将上报地理位置事件推送到开发者填写的URL。
 * 
 * 推送XML数据包示例：
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> 
 * <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[LOCATION]]></Event> 
 * <Latitude>23.137466</Latitude>
 * <Longitude>113.352425</Longitude> 
 * <Precision>119.385040</Precision> </xml>
 * 
 * 参数说明：
 * 
 * 参数 描述<br>
 * ToUserName 开发者微信号<br>
 * FromUserName 发送方帐号（一个OpenID）<br>
 * CreateTime 消息创建时间 （整型）<br>
 * MsgType 消息类型，event<br>
 * Event 事件类型，LOCATION<br>
 * Latitude 地理位置纬度<br>
 * Longitude 地理位置经度<br>
 * Precision 地理位置精度<br>
 * 
 * @author xiezc
 *
 */
public final class LocationEvent extends BaseEvent {

	private double latitude;
	private double longitude;
	private double precision;

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setPrecision(double precision) {
		this.precision = precision;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getPrecision() {
		return precision;
	}

	public LocationEvent(double latitude, double longitude, double precision) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.precision = precision;
		setEvent(EventType.LOCATION);
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addTag("Latitude", String.valueOf(latitude));
		mb.addTag("Longitude", String.valueOf(longitude));
		mb.addTag("Precision", String.valueOf(precision));
		mb.surroundWith("xml");
		return mb.toString();
	}

	

}
