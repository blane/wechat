package org.xiezc.entity.reqMsg.event;

import org.xiezc.util.MessageBuilder;

/**
 * 扫描带参数二维码事件<br>
 * 
 * 用户扫描带场景值二维码时，可能推送以下两种事件：
 * 
 * 如果用户还未关注公众号，则用户可以关注公众号，关注后微信会将带场景值关注事件推送给开发者。
 * 如果用户已经关注公众号，则微信会将带场景值扫描事件推送给开发者。
 * 
 * 1. 用户未关注时，进行关注后的事件推送
 * 
 * 推送XML数据包示例：
 * 
 * <xml><ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[subscribe]]></Event>
 * <EventKey><![CDATA[qrscene_123123]]></EventKey>
 * <Ticket><![CDATA[TICKET]]></Ticket> </xml> 参数说明：
 * 
 * 参数 描述<br>
 * ToUserName 开发者微信号<br>
 * FromUserName 发送方帐号（一个OpenID）<br>
 * CreateTime 消息创建时间 （整型）<br>
 * MsgType 消息类型，event<br>
 * Event 事件类型，subscribe<br>
 * EventKey 事件KEY值，qrscene_为前缀，后面为二维码的参数值<br>
 * Ticket 二维码的ticket，可用来换取二维码图片<br>
 * 
 * 2. 用户已关注时的事件推送
 * 
 * 推送XML数据包示例：
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[SCAN]]></Event> <EventKey><![CDATA[SCENE_VALUE]]></EventKey>
 * <Ticket><![CDATA[TICKET]]></Ticket> </xml>
 * 
 * 参数说明：
 * 
 * 参数 描述<br>
 * ToUserName 开发者微信号<br>
 * FromUserName 发送方帐号（一个OpenID）<br>
 * CreateTime 消息创建时间 （整型）<br>
 * MsgType 消息类型，event<br>
 * Event 事件类型，SCAN<br>
 * EventKey 事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id<br>
 * Ticket 二维码的ticket，可用来换取二维码图片<br>
 */
public final class QrCodeEvent extends BaseEvent {

	private String eventKey;
	private String ticket;

	public String getEventKey() {
		return eventKey;
	}

	public String getTicket() {
		return ticket;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * 
	 * @param eventKey
	 * @param ticket
	 * @param event
	 *            subscribe or SCAN
	 */
	public QrCodeEvent(String eventKey, String ticket, String event) {
		super();
		this.eventKey = eventKey;
		this.ticket = ticket;
		if (EventType.SUBSCRIBE.equals(event) || EventType.SCAN.equals(event)) {
			setEvent(event);
		} else {
			throw new RuntimeException("传入的时间类型参数错误");
		}
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toString());
		mb.addData("EventKey", eventKey);
		mb.addData("Ticket", ticket);
		mb.surroundWith("xml");
		return mb.toString();
	}

}
