package org.xiezc.entity.reqMsg.msg;

import org.xiezc.entity.reqMsg.BaseReq;
import org.xiezc.util.MessageBuilder;

public class BaseReqMsg extends BaseReq {

	String msgId;

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	
	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addTag("MsgId", msgId);
		return mb.toString();
	}

}
