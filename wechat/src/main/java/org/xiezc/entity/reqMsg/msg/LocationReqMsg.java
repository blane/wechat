package org.xiezc.entity.reqMsg.msg;

import org.xiezc.util.MessageBuilder;

/**
 * 
 * 地理位置消息
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1351776360</CreateTime> <MsgType><![CDATA[location]]></MsgType>
 * <Location_X>23.134521</Location_X> <Location_Y>113.358803</Location_Y>
 * <Scale>20</Scale> <Label><![CDATA[位置信息]]></Label>
 * <MsgId>1234567890123456</MsgId> </xml>
 * 
 * 
 * 参数 描述<br>
 * ToUserName 开发者微信号<br>
 * FromUserName 发送方帐号（一个OpenID）<br>
 * CreateTime 消息创建时间 （整型）<br>
 * MsgType location<br>
 * Location_X 地理位置维度<br>
 * Location_Y 地理位置经度<br>
 * Scale 地图缩放大小<br>
 * Label 地理位置信息<br>
 * MsgId 消息id，64位整型<br>
 */
public final class LocationReqMsg extends BaseReqMsg {

	private double locationX;
	private double locationY;
	private int scale;
	private String label;

	public void setLocationX(double locationX) {
		this.locationX = locationX;
	}

	public void setLocationY(double locationY) {
		this.locationY = locationY;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public double getLocationX() {
		return locationX;
	}

	public double getLocationY() {
		return locationY;
	}

	public int getScale() {
		return scale;
	}

	public String getLabel() {
		return label;
	}

	public LocationReqMsg(double locationX, double locationY, int scale, String label) {
		super();
		this.locationX = locationX;
		this.locationY = locationY;
		this.scale = scale;
		this.label = label;
		setMsgType(MsgType.LOCATION);
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", MsgType.LOCATION);
		mb.addTag("Location_X", String.valueOf(locationY));
		mb.addTag("Location_Y", String.valueOf(locationX));
		mb.addTag("Scale", String.valueOf(scale));
		mb.addData("Label", label);
		mb.surroundWith("xml");
		return mb.toString();
	}


}
