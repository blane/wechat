package org.xiezc.entity.reqMsg.msg;

import org.xiezc.util.MessageBuilder;

/**
 * 语音消息
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1357290913</CreateTime> <MsgType><![CDATA[voice]]></MsgType>
 * <MediaId><![CDATA[media_id]]></MediaId> <Format><![CDATA[Format]]></Format>
 * <MsgId>1234567890123456</MsgId> </xml>
 * 
 * 
 * 参数 			描述 										<br>
 * ToUserName 	开发者微信号								<br>
 * FromUserName 发送方帐号（一个OpenID）						<br>
 * CreateTime 	消息创建时间 （整型）							<br>
 * MsgType 		语音为voice								<br>
 * MediaId 		语音消息媒体id，可以调用多媒体文件下载接口拉取数据。 <br>
 * Format 		语音格式，如amr，speex等					<br>
 * MsgID 		消息id，64位整型							<br>
 * 
 * 使用网页调试工具调试该接口									<br>
 * 
 * 
 * 请注意，开通语音识别后，用户每次发送语音给公众号时，				<br>
 * 微信会在推送的语音消息XML数据包中，							<br>
 * 增加一个Recongnition字段（注：由于客户端缓存，开发者开启或者关闭语音识别功能，对新关注者立刻生效，对已关注用户需要24小时生效。
 * 开发者可以重新关注此帐号进行测试）。开启语音识别后的语音XML数据包如下：<br>
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1357290913</CreateTime> <MsgType><![CDATA[voice]]></MsgType>
 * <MediaId><![CDATA[media_id]]></MediaId> <Format><![CDATA[Format]]></Format>
 * <Recognition><![CDATA[腾讯微信团队]]></Recognition>
 * <MsgId>1234567890123456</MsgId> </xml>
 * 
 * 参数说明：<br>
 * 参数 			描述										<br>
 * ToUserName 	开发者微信号								<br>
 * FromUserName 发送方帐号（一个OpenID）						<br>
 * CreateTime 	消息创建时间 （整型）							<br>
 * MsgType 		语音为voice								<br>
 * MediaID 		语音消息媒体id，可以调用多媒体文件下载接口拉取该媒体	<br>
 * Format 		语音格式：amr								<br>
 * Recognition 	语音识别结果，UTF8编码						<br>
 * MsgID 		消息id，64位整型							<br>
 */
public final class VoiceReqMsg extends BaseReqMsg {

	private String mediaId;
	private String format;
	private String recognition;

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

	public String getMediaId() {
		return mediaId;
	}

	public String getFormat() {
		return format;
	}

	public String getRecognition() {
		return recognition;
	}

	
	public VoiceReqMsg(String mediaId, String format, String recognition) {
		super();
		this.mediaId = mediaId;
		this.format = format;
		this.recognition = recognition;
		setMsgType(MsgType.VOICE);
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", MsgType.VOICE);
		mb.addData("MediaId", mediaId);
		mb.addData("Format", format);
		mb.surroundWith("xml");
		return mb.toString();
	}


}
