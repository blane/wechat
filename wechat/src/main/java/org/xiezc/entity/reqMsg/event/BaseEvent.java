package org.xiezc.entity.reqMsg.event;

import org.xiezc.entity.reqMsg.BaseReq;
import org.xiezc.entity.reqMsg.msg.MsgType;
import org.xiezc.util.MessageBuilder;

public class BaseEvent extends BaseReq {

	private String event;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public BaseEvent() {
		setMsgType(MsgType.EVENT);
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", MsgType.EVENT);
		mb.addData("Event", event);
		return mb.toString();
	}

}
