package org.xiezc.entity.reqMsg.event;

import org.xiezc.util.MessageBuilder;

/**
 * 关注/取消关注事件<br>
 * 
 * 用户在关注与取消关注公众号时，微信会把这个事件推送到开发者填写的URL。方便开发者给用户下发欢迎消息或者做帐号的解绑。<br>
 * 
 * 微信服务器在五秒内收不到响应会断掉连接，并且重新发起请求，总共重试三次<br>
 * 
 * 关于重试的消息排重，推荐使用FromUserName + CreateTime 排重。<br>
 * 
 * 假如服务器无法保证在五秒内处理并回复，可以直接回复空串，微信服务器不会对此作任何处理，并且不会发起重试。<br>
 * 
 * 推送XML数据包示例：<br>
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[FromUser]]></FromUserName>
 * <CreateTime>123456789</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[subscribe]]></Event> </xml> 参数说明：<br>
 * 
 * 参数 描述<br>
 * ToUserName 开发者微信号<br>
 * FromUserName 发送方帐号（一个OpenID）<br>
 * CreateTime 消息创建时间 （整型）<br>
 * MsgType 消息类型，event<br>
 * Event 事件类型，subscribe(订阅)、unsubscribe(取消订阅)<br>
 * 
 * @author xiezc
 * @date 2016年7月14日 下午9:35:59
 */
public final class SubscribeEvent extends BaseEvent {

	/**
	 * 
	 * @param event
	 *            subscribe：订阅；unsubscribe：取消订阅。
	 */
	public SubscribeEvent(String event) {
		if (EventType.SUBSCRIBE.equals(event) || EventType.UNSUBSCRIBE.equals(event)) {
			setEvent(event);
		} else {
			throw new RuntimeException("传入的时间类型参数不对");
		}

	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.surroundWith("xml");
		return mb.toString();
	}

}
