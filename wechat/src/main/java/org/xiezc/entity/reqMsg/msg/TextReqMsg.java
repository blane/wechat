package org.xiezc.entity.reqMsg.msg;

import org.xiezc.util.MessageBuilder;

/**
 * 文本消息
 * 
 * <xml> 
 * <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1348831860</CreateTime> <MsgType><![CDATA[text]]></MsgType>
 * <Content><![CDATA[this is a test]]></Content>
 * <MsgId>1234567890123456</MsgId>
 * </xml>
 * 
 * 
 * 参数                                 描述                                            <br/>
 * ToUserName      开发者微信号			<br/>
 * FromUserName    发送方帐号（一个OpenID）   <br/>
 * CreateTime      消息创建时间 （整型）		<br/>
 * MsgType         text					<br/>
 * Content         文本消息内容			<br/>
 * MsgId           消息id，64位整型		<br/>
 */
public final class TextReqMsg extends BaseReqMsg {

	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public TextReqMsg(String content) {
		super();
		this.content = content;
		setMsgType(MsgType.TEXT);
	}


	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", MsgType.TEXT);
		mb.addData("Content", content);
		mb.surroundWith("xml");
		return mb.toString();
	}


}
