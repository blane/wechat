package org.xiezc.entity.reqMsg.msg;

import org.xiezc.util.MessageBuilder;

/**
 * 链接消息
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1351776360</CreateTime> 
 * <MsgType><![CDATA[link]]></MsgType>
 * <Title><![CDATA[公众平台官网链接]]></Title>
 * <Description><![CDATA[公众平台官网链接]]></Description> 
 * <Url><![CDATA[url]]></Url>
 * <MsgId>1234567890123456</MsgId> </xml>
 * 
 * 
 * 参数 描述<br>
 * ToUserName 接收方微信号<br>
 * FromUserName 发送方微信号，若为普通用户，则是一个OpenID<br>
 * CreateTime 消息创建时间<br>
 * MsgType 消息类型，link<br>
 * Title 消息标题<br>
 * Description 消息描述<br>
 * Url 消息链接<br>
 * MsgId 消息id，64位整型<br>
 * 
 * @author xiezc
 *
 */
public final class LinkReqMsg extends BaseReqMsg {

	private String title;
	private String description;
	private String url;

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

	public LinkReqMsg(String title, String description, String url) {
		super();
		this.title = title;
		this.description = description;
		this.url = url;
		setMsgType(MsgType.EVENT);
	}

	
	
	

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", MsgType.LINK);
		mb.addData("Title", title);
		mb.addData("Description", description);
		mb.addData("Url", url);
		mb.surroundWith("xml");
		return mb.toString();
	}


}
