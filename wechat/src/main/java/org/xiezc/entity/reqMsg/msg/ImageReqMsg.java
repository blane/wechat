package org.xiezc.entity.reqMsg.msg;

import org.xiezc.util.MessageBuilder;

/**
 * 图片消息
 * 
 * <xml><ToUserName><![CDATA[gh_680bdefc8c5d]]></ToUserName>
 * <FromUserName><![CDATA[oIDrpjqASyTPnxRmpS9O_ruZGsfk]]></FromUserName>
 * <CreateTime>1359028479</CreateTime> <MsgType><![CDATA[image]]></MsgType>
 * <PicUrl>
 * <![CDATA[http://mmbiz.qpic.cn/mmbiz/L4qjYtOibummHn90t1mnaibYiaR8ljyicF3MW7XX3BLp1qZgUb7CtZ0DxqYFI4uAQH1FWs3hUicpibjF0pOqLEQyDMlg/0]]>
 * </PicUrl> <MsgId>5836982871638042400</MsgId> <MediaId>
 * <![CDATA[PGKsO3LAgbVTsFYO7FGu51KUYa07D0C_Nozz2fn1z6VYtHOsF59PTFl0vagGxkVH]]>
 * </MediaId> </xml>
 * 
 * 
 * 参数 描述 <br>
 * ToUserName 开发者微信号 <br>
 * FromUserName 发送方帐号（一个OpenID） <br>
 * CreateTime 消息创建时间 （整型） <br>
 * MsgType image <br>
 * PicUrl 图片链接（由系统生成） <br>
 * MediaId 图片消息媒体id，可以调用多媒体文件下载接口拉取数据。 <br>
 * MsgId 消息id，64位整型<br>
 */
public final class ImageReqMsg extends BaseReqMsg {

	private String picUrl;
	private String mediaId;

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public String getMediaId() {
		return mediaId;
	}

	public ImageReqMsg(String picUrl, String mediaId) {
		super();
		this.picUrl = picUrl;
		this.mediaId = mediaId;
		setMsgType(MsgType.IMAGE);
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", MsgType.IMAGE);
		mb.addData("PicUrl", picUrl);
		mb.addData("MediaId", mediaId);
		mb.surroundWith("xml");
		return mb.toString();
	}

}
