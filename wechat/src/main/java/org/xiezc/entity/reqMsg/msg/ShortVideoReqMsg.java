package org.xiezc.entity.reqMsg.msg;

import org.xiezc.util.MessageBuilder;

/**
 * 小视频消息
 * 
 * <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1357290913</CreateTime> <MsgType><![CDATA[shortvideo]]></MsgType>
 * <MediaId><![CDATA[media_id]]></MediaId>
 * <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
 * <MsgId>1234567890123456</MsgId> </xml>
 * 
 * 
 * 参数 描述 <br>
 * ToUserName 开发者微信号<br>
 * FromUserName 发送方帐号（一个OpenID）<br>
 * CreateTime 消息创建时间 （整型）<br>
 * MsgType 小视频为shortvideo<br>
 * MediaId 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。<br>
 * ThumbMediaId 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。<br>
 * MsgId 消息id，64位整型<br>
 * 
 * @author xiezc
 * @date 2016年7月13日 下午11:16:39
 */
public class ShortVideoReqMsg extends BaseReqMsg {

	private String mediaId;
	private String thumbMediaId;

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public String getMediaId() {
		return mediaId;
	}

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public ShortVideoReqMsg(String mediaId, String thumbMediaId) {
		super();
		this.mediaId = mediaId;
		this.thumbMediaId = thumbMediaId;
		setMsgType(MsgType.VIDEO);
	}

	@Override
	public String toXml() {
		MessageBuilder mb = new MessageBuilder(super.toXml());
		mb.addData("MsgType", MsgType.SHORTVIDEO);
		mb.addData("MediaId", mediaId);
		mb.addData("ThumbMediaId", thumbMediaId);
		mb.surroundWith("xml");
		return mb.toString();
	}

}
