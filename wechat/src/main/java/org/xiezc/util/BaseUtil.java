package org.xiezc.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import org.xiezc.InitializeListerner;

public class BaseUtil {

	private static Logger logger = Logger.getLogger(BaseUtil.class);

	/**
	 * get project root path
	 * 
	 * @return String rootPath
	 */
	public String getRootPath() {
		String rootPath = BaseUtil.class.getClassLoader().getResource("").getPath();
		return rootPath;
	}

	

	/**
	 * get newest accesstoken
	 * 
	 * @return
	 */
	public static String getAccessToken() {
		return InitializeListerner.wechatConfig.updateAccessToken();
	}

	/**
	 * send request to url by method GET and no param
	 * 
	 * @param url
	 * @return result
	 */
	public static String sendGet(String url) {
		return BaseUtil.sendGet(url, null);
	}

	/**
	 * send request to url by method GET
	 * 
	 * @param url
	 * @param param
	 *            name1=value1&name2=value2
	 * @return result
	 */
	public static String sendGet(String url, String param) {
		// 校验
		if (StrUtil.isNOB(url)) {
			throw new RuntimeException("the url is empty or null(url是空的)");
		}

		StringBuffer result = new StringBuffer();
		BufferedReader in = null;
		try {
			String urlNameString = url.trim();
			if (!StrUtil.isNOB(param)) {
				urlNameString = urlNameString + "?" + param.trim();
			}

			URL realUrl = new URL(urlNameString);

			// Open connection between the URL
			URLConnection connection = realUrl.openConnection();

			// Set the request of the general properties
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

			// Open connection
			connection.connect();

			// Set BufferedReader input stream to read response
			in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
			logger.error("Send GET request failure\n" + e.toString() + "\n" + url);
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result.toString();
	}

	/**
	 * send request to url by method POST
	 * 
	 * @param url
	 * @param param
	 * @return result
	 */
	public static String sendPost(String url, String param) {
		PrintWriter out = null;
		BufferedReader in = null;
		StringBuffer result = new StringBuffer();
		try {
			if (StrUtil.isNOB(url)) {
				throw new RuntimeException("发送的url是空的");
			}
			URL realUrl = new URL(url);

			// Open connection between the URL
			URLConnection conn = realUrl.openConnection();

			// Set the request of the general properties
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			try {
				new JSONObject(param);
				conn.setRequestProperty("Content-Type", "application/json");
			} catch (JSONException e) {
				// do not set content-type
			}

			// Send POST request must be set
			conn.setDoOutput(true);
			conn.setDoInput(true);

			// Get URLConnection object output stream
			out = new PrintWriter(conn.getOutputStream());

			// Send param
			out.print(param);

			// flush output stream buffer
			out.flush();

			// Set BufferedReader input stream to read response
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
			logger.error("Send POST request failure\n" + e.toString() + "\n" + url);
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result.toString();
	}

}
