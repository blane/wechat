package org.xiezc.util;

import org.json.JSONObject;
import org.xiezc.InitializeListerner;
import org.xiezc.entity.WechatConfig;

import com.wechat.model.User;

/**
 * 此类主要用来访问微信的常用接口
 * 
 * @author xiezc
 * @date 2016年7月16日 下午10:59:14
 */
public class WechatInterfaceUtil {

	private static WechatConfig wechatConfig = InitializeListerner.wechatConfig;

	/**
	 * 获取用户基本信息（包括UnionID机制）<br>
	 * 开发者可通过OpenID来获取用户基本信息。请使用https协议。<br>
	 * 接口调用请求说明<br>
	 * http请求方式: GET
	 * https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&
	 * openid=OPENID&lang=zh_CN<br>
	 * 参数说明<br>
	 * 参数 是否必须 说明<br>
	 * access_token 是 调用接口凭证<br>
	 * openid 是 普通用户的标识，对当前公众号唯一<br>
	 * lang 否 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语<br>
	 * 返回说明<br>
	 * 正常情况下，微信会返回下述JSON数据包给公众号：<br>
	 * {<br>
	 * "subscribe": 1,<br>
	 * "openid": "o6_bmjrPTlm6_2sgVt7hMZOPfL2M",<br>
	 * "nickname": "Band", <br>
	 * "sex": 1, <br>
	 * "language": "zh_CN",<br>
	 * "city": "广州",<br>
	 * "province": "广东", <br>
	 * "country": "中国",<br>
	 * "headimgurl": "http://wx.qlogo.cn/mmopen/
	 * g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4
	 * <br>
	 * eMsv84eavHiaiceqxibJxCfHe/0",<br>
	 * "subscribe_time": 1382694957,<br>
	 * "unionid": " o6_bmasdasdsad6_2sgVt7hMZOPfL"<br>
	 * "remark": "",<br>
	 * "groupid": 0,<br>
	 * "tagid_list":[128,2]<br>
	 * }<br>
	 * 参数说明<br>
	 * 参数 说明<br>
	 * subscribe 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。<br>
	 * openid 用户的标识，对当前公众号唯一<br>
	 * nickname 用户的昵称<br>
	 * sex 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知<br>
	 * city 用户所在城市<br>
	 * country 用户所在国家<br>
	 * province 用户所在省份<br>
	 * language 用户的语言，简体中文为zh_CN<br>
	 * headimgurl
	 * 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。
	 * 若用户更换头像，原有头像URL将失效。<br>
	 * subscribe_time 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间<br>
	 * unionid 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。<br>
	 * remark 公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注<br>
	 * groupid 用户所在的分组ID（兼容旧的用户分组接口）<br>
	 * tagid_list 用户被打上的标签ID列表<br>
	 * 错误时微信会返回错误码等信息，JSON数据包示例如下（该示例为AppID无效错误）:<br>
	 */
	public static User getUser(String openId) {
		String url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=" + wechatConfig.getAccessToken()
				+ "&openid=" + openId + "&lang=zh_CN";
		String result = BaseUtil.sendGet(url);
		JSONObject jsonObject = new JSONObject(result);
		User user = new User();
		user.setSubscribe(jsonObject.getInt("subscribe"));
		user.setOpenId(openId);
		user.setNickName(jsonObject.getString("nickname"));
		user.setSex(jsonObject.getInt("sex"));
		user.setCity(jsonObject.getString("city"));
		user.setCountry(jsonObject.getString("country"));
		user.setProvince(jsonObject.getString("province"));
		user.setLanguage(jsonObject.getString("language"));
		user.setHeadImgUrl(jsonObject.getString("headimgurl"));
		user.setSubscribeTime(jsonObject.getLong("subscribe_time"));
		user.setRemark(jsonObject.getString("remark"));
		user.setGroupId(jsonObject.getInt("groupid"));
		user.setTagidList(jsonObject.getString("tagid_list"));
		return user;
	}

}
