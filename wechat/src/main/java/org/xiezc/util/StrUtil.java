package org.xiezc.util;

public class StrUtil {

	/**
	 * 判断字符串是null或者空白
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNOB(String str) {
		if (str == null || "".equals(str)) {
			return true;
		}
		return false;
	}

}
